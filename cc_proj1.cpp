/* Header files */

#include<iostream>
#include<string>
#include<stdlib.h>
#include<fstream>
using namespace std;

/* Fuction Prototypes */
void create_token(string,string);
void hash_mapping(int arr[256]);

int main()
{
	int state_table[9][5]={0};
	int mapping[256]={0};
	hash_mapping(mapping);
	ifstream inFile;
	inFile.open("transition_table.csv",ios::in);
	if(!inFile.is_open())
	{
		exit(0);
	}
	char x;
	for(int i=1;i<9;i++)
	{
		for(int j=0;j<5;j++)
		{
			inFile>>x;
			state_table[i][j]=x-48;
		}
	}
	inFile.close();
	for(int i = 0; i<9;i++)
	{
		for (int j=0;j<5;j++)
			cout<<state_table[i][j]<<"\t";
		cout<<endl;
	}
	cout<<endl;

	inFile.open("test.txt",ios::in);
	if(!inFile.is_open())
	{
		exit(0);
	}
	string instruction;
	int state = 1;
	string lexeme = "";
	while(!inFile.eof())
	{
		getline(inFile,instruction);

		for(int i = 0; instruction[i]!='\0';i++)
		{
			/*cout<<"Current State::"<<state<<endl;
			cout<<"Current Char::"<<instruction[i]<<endl;
			cout<<"====================================="<<endl;*/
			switch(state)
			{
				case 1:
				cout<<"Current Char::"<<instruction[i]<<endl;
				lexeme+=instruction[i];
				state = state_table[state][mapping[int(instruction[i])]];
				cout<<"Next State::"<< state<<endl;
				break;
				case 2:
				state = state_table[state][mapping[int(instruction[i])]];
				if(state==1)
				{
					cout<<"inside if"<<endl;
					cout<<lexeme<<endl;
					cout<<"=-=-=-=-=-=-=-=-=-=-=-=-="<<endl;
					lexeme = "";
				}
				lexeme+=instruction[i];
				break;
				case 3:
				cout<<"Current Char::"<<instruction[i]<<endl;
				state = state_table[state][mapping[int(instruction[i])]];
				cout<<"Next State::"<< state<<endl;
				if(state==1)
				{
					cout<<"inside if"<<endl;
					cout<<lexeme<<endl;
					lexeme = "";
					i--;
				}
				else
				{
					lexeme+=instruction[i];
				}
				break;
				case 4:
				state = state_table[state][mapping[instruction[i]]];
				cout<<"Next State::"<< state<<endl;
				if(state == 1)
				{
					cout<<"inside if"<<endl;
					cout<<lexeme<<endl;
					lexeme = "";
					i--;
				}
				else
				{
					lexeme+=instruction[i];
				}
				break;
				case 5:
				break;
				case 6:
				break;
				case 7:
				break;
				case 8:
				break;
				case 9:
				break;
				case 10:
				break;
			}
			cout<<"========================================================="<<endl;
		}
		cout<<lexeme<<endl;
		cout<<"========================================================="<<endl;
	}
	cout<<int('_')<<endl;
	return 0;
}




void hash_mapping(int arr[256])
{
	for (int i=0;i<255;i++)
	{
		if(char(i)=='_')
			arr[i] = 0;
		if(char(i)>='a' && char(i)<='z')
			arr[i]=1;
		if(char(i)>='A' && char(i)<='Z')
			arr[i]=1;
		if(char(i)>='0' && char(i)<='9')
			arr[i]=2;
		if(char(i)=='+')
			arr[i]=3;
	}
	cout<<"Plus::"<<int('+')<<endl;
	cout<<arr[int('+')]<<endl;
	cout<<arr[43]<<endl;
}